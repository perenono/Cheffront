export class Person {

  constructor(
    public id: number,
    public firstname: string,
    public lastname: string,
    public login: string,
    public password: string,
    public role: string
  ) { }

}
