import { Component } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

import { Person } from '../person';
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-person-form',
  templateUrl: './person-form.component.html',
  styleUrls: ['./person-form.component.css']
})
export class PersonFormComponent {

  private readonly URL = 'https://localhost:8443/person';

  roles = [
    { id: "ADMIN", name: "Administrator" },
    { id: "CHEF", name: "Chef" },
    { id: "USER", name: "User" }
  ];

  model = new Person(18, 'noel', 'perez', 'nperez', '1234', 'CHEF');

  submitted = false;

  constructor(private http: HttpClient) {
  }

  onSubmit() {
    this.submitted = true;

    console.log('Request is sent!');
    console.log('model:'+JSON.stringify(this.model));
    // Using the POST method
    const headers =  new  HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic dXNlckFkbWluOnVzZXJBZG1pbg=='})   //userAdmin
    ;

    let params = new HttpParams();
    params = params.append('firstname', this.model.firstname);
    params = params.append('lastname', this.model.lastname);
    params = params.append('login', this.model.login);
    params = params.append('password', this.model.password);
    params = params.append('role', this.model.role);

    const text_return = this.http.post<any>(this.URL,
      {},
      {params,
        headers: headers
      }).subscribe({
      next: data => {
        console.log("success");
      },
      error: error => {
        console.error('There was an error!', error);
      }
    });
//    console.log('result:'+JSON.stringify(text_return));
  }


  newPerson() {
    this.model = new Person(42, '', '', '', '', '');
  }

  skyDog(): Person {
    const myPerson =  new Person(42, 'noel',
                           'perez','nperez','1234', 'CHEF');
    console.log('My person is called ' + myPerson.login); // "My hero is called SkyDog"
    return myPerson;
  }

  //////// NOT SHOWN IN DOCS ////////

  // Reveal in html:
  //   Name via form.controls = {{showFormControls(heroForm)}}
  showFormControls(form: any) {
    return form && form.controls.name &&
    form.controls.name.value; // Dr. IQ
  }

  /////////////////////////////

}
