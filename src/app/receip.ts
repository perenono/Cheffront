export class Receip {

  constructor(
    public id: number,
    public title: string,
    public instruction: string
  ) {  }

}
