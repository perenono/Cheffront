import { Component } from '@angular/core';

import { Receip } from '../receip';

@Component({
  selector: 'app-receip-form',
  templateUrl: './receip-form.component.html',
  styleUrls: ['./receip-form.component.css']
})
export class ReceipFormComponent {

  powers = ['Really Smart', 'Super Flexible',
            'Super Hot', 'Weather Changer'];

  model = new Receip(18, 'Dr IQ', 'Chuck Overstreet');

  submitted = false;

  onSubmit() { this.submitted = true; }


  newReceip() {
    this.model = new Receip(42, '', '');
  }

  skyDog(): Receip {
    const myReceip =  new Receip(42, 'SkyDog',
                           'Fetch any object at any distance');
    console.log('My hero is called ' + myReceip.title); // "My hero is called SkyDog"
    return myReceip;
  }

  //////// NOT SHOWN IN DOCS ////////

  // Reveal in html:
  //   Name via form.controls = {{showFormControls(heroForm)}}
  showFormControls(form: any) {
    return form && form.controls.name &&
    form.controls.name.value; // Dr. IQ
  }

  /////////////////////////////

}
